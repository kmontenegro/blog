---
title: About Ken
subtitle: Movement technologist and lawyer
comments: false
---

My name is Ken Montenegro. I'm a technologist with a law degree. My work is largely focused on building more robust social movement.

I do boutique technology consulting which you can learn about at my [website](https://www.comeuppance.net).
### Affiliaitons  
* National Vice President, [National Lawyers Guild](https://www.nlg.org)
* Board Member, [NTEN](https://www.nten.org)  
* Board Member, [Immigrant Defenders Law Center](https://www.immdef.org)  
* Co-founder, [Stop LAPD Spying Coalition](https://stoplapdspying.org)  
* Founding Member, [Radical Connections](https://radicalconnections.net) 
* Advisor, [Social Movement Technologies](https://www.socialmovementtechnologies.org)    