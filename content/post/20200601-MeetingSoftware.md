---
title: Meeting Software  
subtitle: What should we use?  
date: 2020-06-01  
tags: ["collaboration", "security"]  
---  

# Meeting Software/Services For Movement Work  
## Context  
As remote meeting solutions are critical for work currently happening in the midst of a global pandemic, the recurring question is "what should I use?!?". The answer to that is complicated. Various factors will come into play. This short note is not exhaustive but broaches some considerations.  
## Solutions  
### Jitsi  
Jitsi is one of the more secure solutions for web-based meetings. It runs from Chrome, Chromium, Brave, and MS Edge web-browsers. It doesn't run as well using Firefox or other web-browsers. Its code can be reviewed so there are no surprises like what happened with Zoom claiming end-to-end encryption when they were really doing transport layer security (i.e. not what they claimed). Most Jitsi instances are not collecting data about its users in a way that is sketchy.  

Jitsi is currently owned by 8x8 who offer free meetings. There are more security conscious Jitsi instances at:   

* [MayFirst Movement Technology](https://meet.mayfirst.org): MayFirst exists to support movements and is a membership organization. 
* [Jitsi](https://meet.jit.si): Jitsi is a software company owned by 8x8 incorporated that works/builds Jitsi but it is not the commercial arm/entity.  
* [Nitrokey](https://meet.nitrokey.com): Nitrokey is a technology company that creates, generally, secure hardware tokens.  

#### Jitsi Considerations  
* The first person in the Jitsi meeting becomes the moderator that means they effectively own the meeting; 
* Not all Jitsi instances are adequate for large meetings because Jitsi lacks many web-meeting controls;
* Jitsi instance do not give you a dial-in number. That means that Jitsi is not a good choice if people need to dial in. That said, part of good security is encouraging folks to use the same tools (e.g. a dial in number breaks any encryption you thought existed for a meeting)  
* User/guest management is something Jitsi needs to work on: right now you cannot definitively kick/eject a user from your room/meeting;  
* Jitsi's chat feature often takes too much space from the main pane obscuring guest/attendees under it;  
* Jitsi is not good webinar software; while it's good for meetings, it is not made for webinars;
* Because Jitsi is creating individual video and audio streams per person/user, a bad internet connection from one person will impact the entire meeting;
* The Jitsi instance at [8x8](https://www.8x8.com) allows you to do better scheduling and have some controls. While it's free, it is unclear how much information they are collecting from the users. As a corporation, 8x8 is likely lots more likely to collect user data than the providers listed above.  

#### Jitsi Verdict  
Jitsi is a very good platform for smaller meetings when you know the people you're meeting with. The fact that there is no phone dial in option can be seen as a good security feature more than a limitation. It is also nice that there is no user software to install which reduces the things that could go wrong. When folks are experiencing issues with quality, users can turn their cameras off and the meeting quality usually improves.   

In short, Jitsi is very good secure meeting software.  
### Zoom  
Zoom rules everything around me (Z.R.E.A.M.). While it has become the de facto meeting platform during the pandemic, it has also been exposed to overdue scruitiny about its privacy and security. It, Zoom is a mess in these critical areas. That said, Zoom's commercial competitors would probably crumble were similar scrutiny applied to their products.   
While we won't go into detail about Zoom's failures, it's important to recognize that it is dominant technology. That means that anyone doing web meetings must factor Zoom's benefits when selecting a platform.  
#### Zoom Benfits  
* Ubiquity...everyone uses it or has used it. There's something to be said about meeting users where they are at;
* Security: Zoom is increasing its security by allowing you to select data centers and has expressed soon they will offer true end-to-end encryption. Unfortunately, the rumor is that these will be available only for paying customers (>$140/year);
* Accounts: Zoom users can create accounts which save things like meeting preferences. Additionally, Zoom allows federated login; that means you can login with your Google/GMail and other accounts. This reduces the passwords folks need to remember so it decreases password risk;   
* Scheduling: Zoom has very good scheduling tools;  
* Recordings: Zoom makes it drop-dead easy to record meetings;
* Webinars: Zoom allows you to upgrade to a webinar product (e.g. Question and Answer capacity; attendees having "privacy" from being seen; registration pages, etc).  

### Wire  
FORTHCOMING   

### BigBlueButton
[BBB](https://www.bigbluebutton.org) is a new contender in the space: it is free and open source and created to schools. Folks can sign up and use an account on the site for now but eventually there will be public instances that folks can use moving forward. This platform is the closest to webinar capacity in the free and open source space.

### Live.Mayfirst.org  
If you want to broadcast, e.g. one-way, then the [Mayfirst](https://live.mayfirst.org) option is worth taking a look at. One challenge is that it is single camera with no engagement (chat etc) options. This is not webinar software but something good for a one-person panel or report back/presentation.    

## TL;DR/Summary  
Jitsi is a very good option for smaller meetings with people you trust and can attend from in front of a computer. It is both secure and has essential meeting features.   
Zoom is good for larger meetings where you might need to limit things like chat and want to kick people out of a web meeting. Zoom is also a good webinar platform though, across all its product lines, there are privacy issues that folks should pay attention to.  

### Post-data  
What would it look like if we actually had movement technology designed by organizers, healers, movement lawyers, movement acamedicians? I'm looking forward to that day! 