---
title: Devices We Often Overlook
subtitle: Some are so visible that we forget them...
date: 2019-08-04
tags: ["network", "security"]
---

Last week, I was reviewing some networked fax devices and was surprised by how few of the device manufacturers ever update the software on their devices. Mind you, these were all networked fax devices so it was not an ancient and arcane "this will never connect to a network so update by USB" process. It is the manufacturer counting on users not stopping to think about the threat these devices could pose and, therefore, feeling no compulsion to update them. Coupled with how some systems are invisibly baked-into our management process that we forget to update them.  

## Key Examples  
1- Routers: smaller and medium offices are not very good about updating router/firewall software. Most feel that because the internet is working fine, their router must be. This approach could potentially expose an entire office network.  
2- Copiers: copiers are full-blown computers that can email, perform optical character recognition, store documents, etc. Most copiers are leased and this complicates the issue more because most leassors are not concerned about your network or data security.  
3- Printers: Printers can present a threat to your network. I'm a fan of turning off protocols you aren't going to use and buying from a larger manufacturer as they might have an incentive to update their software.   
4- Access systems: if you have a smart-lock, key-card, or other access controll system that interfaces with a computer network, update it and lock it down.  
5- HVAC: update those systems and put them on their own network...I think this is a critical lesson from the Target hack.  
6- Fax machines: yes, these machines, if connected to your network, can be an attack vector. The impetus for this short note is [here](https://www.healthcareitnews.com/news/fax-machines-can-be-hacked-breach-network-using-only-its-number).  
7- Phone systems: I don't know what to say about phone systems but those that still have them on-prem should be resourced to make sure they're patched. An office I know had a phone-spend spike; when I helped them look at it, the root cause was someone dialing into voicemail, hacking a voicemail box, and then using that VM box to make outgoing international calls!    

Every shield has an edge; but understand the edge or size of your shield(s).  