---
title: Unicorns and Secure Collaboration 
subtitle: Security is hard to scale
date: 2019-05-23
---

## Is There Such A Thing As Secure Collaboration?  
While collaboration is often praise, most of the praise elides the fact that collaboration will always be at odds with security. We can use the oral transmission of secrets as an example: when you've shared a secret with one person, it is easier to track if and when the secret was disclosed; once you've shared it with more than two people, tracking the disclosure becomes exponentially more difficult.  