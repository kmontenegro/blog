---
title: Cursory Travel Safety
subtitle: These are by no means exhaustive
date: 2019-08-17
tags: ["travel", "security"]
---  

It might seem obvious but these are times when crossing a border presents challenges to certain folks. Here are some back of the napkin ideas about travel precautions.  

1. Decide whether there's a purpose to broadly broadcasting your travel plans;
2. Let trusted folks on both ends of the travel conduit know your itinerary;  
3. Create a check-in procedure with folks so that it's clear that you landed, got off the conveyance, made it to customs, and exited customs;  
4. If you are subjected to a device search, and get your device back, do not trust that device (if you have a faraday bag, use it);  
	a. make a note of who took your device  
	b. how long did they have it  
	c. is your device behaving oddly after its return  
	d. contact someone in your circle who can contact the [Electronic Frontier Foundation](https://www.eff.org), [Citizen Lab](https://citizenlab.ca/), or [Access Now](https://www.accessnow.org/) to identify next steps.  
5. Once you are admitted, or as you're being deported, update the circle of trusted folks so they can curtail or amplify their actions in your favor;  
6. Be gentle with yourself as a border search has more to do with the inherent violence of borders than anything you did.  