---
title: WFH Equipment  
subtitle: this is my work from home setup  
date: 2020-05-24  
tags: ["wfh", "security"]  
---

I thought it would be helpful to share what my remote work setup.  

This setup will not work for everyone as each WFH situation is different. I want to recognize that space will play a big factor in how fleshed out a WFH setup can be mainly because a good setup isn't one that can be broken-down or put away easily.  

### Current Setup (some items are >4 years old)  
* [Dell U2425](https://www.newegg.com/dell-u2415-24) monitor  
* [Anker USB-3](https://www.anker.com/uk/products/variant/usb-30-docking-station/68ANDOCKS-BKA) docking station  
* [Logitech K480](https://www.logitech.com/en-us/product/multi-device-keyboard-k480) bluetooth keyboard
* [Logitech M510](https://www.logitech.com/en-us/product/wireless-mouse-m510) USB wireless mouse
* [3M LX550](https://www.newegg.com/3m-drawer-cabinets/p/N82E16848021304) laptop stand  
* [Doss Soundbox](https://www.dossaudio.com/products/soundbox) USB & bluetooth speaker  
* [Logitech C920-C](https://www.amazon.com/Logitech-960-000945-Webcam-C920-C/dp/B00BUJBHGG) USB webcam   
* [Apricom 64GB](https://apricorn.com/aegis-secure-key-3z?gclid=EAIaIQobChMI6KrD7pbT6QIVmcDICh1JdQHfEAAYASAAEgJSnfD_BwE) encrypted USB drive  
* [Yubikey](https://www.yubico.com/product/yubikey-5c) keys  
* [APC UPS](https://www.newegg.com/apc-be425m-6-nema-5-15-r-outlets-battery-backup-4-surge-outlets-2) battery backup  
* *ON ORDER:* [Xerox B210](https://www.newegg.com/xerox-b210-dni) laser printer
 
### Items I'm Missing From LA  
In my Los Angeles home office, I had a two Synology NAS devices that I wish were here for some virtualization work. But for everyday stuff, things I'm missing are:  
* Amazon Basics cross-cut shredder  
* External DVD drive (I became aware that I don't have any optical drives in my house)  
* Side table (I effectively had an L-shaped work setup)  
* Samsung laser printer (I like to print anything over 7-10 pages that needs to be read/reviewed with detail)  
* Eero wireless mesh network (It would give me exceptional wifi coverage throughout the lot my house sits on)

### Things We'd Do Differently  
1. USB-C dock by [CalDigit](https://www.bhphotovideo.com/c/product/1525416-REG/caldigit_usbcprodock_us07_sg_thunderbolt_usb_c_pro_dock.html)  
2. Keyboard with a longer layout by [Logitech](https://www.newegg.com/logitech-k780-920-008149-usb-bluetooth-wireless/p/N82E16823126476)   
3. Ergonomic mouse by [Anker](https://www.newegg.com/p/173-0091-00004)  
2. 27" Monitor by [Dell](https://www.newegg.com/dell-p2719h-27-full-hd)    
3. UPS by [Cyberpower](https://www.newegg.com/cyberpower-cp550slg-nema-5-15r)    
4. Autonomous [standing desk](https://www.autonomous.ai/standing-desks/smartdesk-2-home) w/ stand-alone file cabinet 

I'd add that having a good desk and chair are fundamental. My chair is from [Autonomous.ai](https://www.autonomous.ai/office-chairs/ergonomic-chair) & my desk is a convertible sit/stand from [Wayfair](https://www.wayfair.com/furniture/pdp/beachcrest-home-pinellas-height-adjustable-standing-desk-bcmh3392.html). 