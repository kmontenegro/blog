---
title: Office 365 Security Tools
subtitle: These tools make a difference
date: 2018-09-20
tags: ["O365", "security"]
---

Microsoft has really caught up and arguably overtaken other cloud platform vendors when it comes to creating a secure environment.  

It seems like each release cycle greater account control and data protections.  

While these are great and accessible for smaller groups with limited staff, it's important to remember that Microsoft is not you or your organization's friend. It's a business transaction.  

### Tools  

* [Microsoft Security Graph](
https://www.microsoft.com/en-us/security/intelligence)
  
* [Azure Information Protection Add-in](
https://www.microsoft.com/en-us/download/details.aspx?id=53018)    

* [O365 Secure Score]( 
https://docs.microsoft.com/en-us/office365/securitycompliance/office-365-secure-score)   

* [Enterprise Mobility and Security for Nonprofits]( 
https://www.microsoft.com/en-us/nonprofits/enterprise-mobility-security)    

* [O365 Attack Simulator]( 
https://docs.microsoft.com/en-us/office365/securitycompliance/attack-simulator)   

