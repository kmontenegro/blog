## Ken Montenegro Notes

It's an experiemental place where plan to house public notes and move off of Medium.

If you'd like to learn more about me or my work, please visit my main [website](https://www.comeuppance.net).  

<sub>This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/) & [Hugo](https://gohugo.io) and published with [Netlify](https://www.netlify.com). It can be built in under 2 minutes.</sub>  

[![Netlify Status](https://api.netlify.com/api/v1/badges/1cea626d-de3c-4b85-8899-9ea190dfc441/deploy-status)](https://app.netlify.com/sites/comeuppance/deploys)
